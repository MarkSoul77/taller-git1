const { Router } = require("express");
const { holaMundo } = require("../controllers/hola-mundo");

const router = Router();

router.get("/hola-mundo", holaMundo);

module.exports = router;
